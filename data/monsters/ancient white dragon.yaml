ability_mods:
  cha_mod: 3
  con_mod: 7
  dex_mod: 3
  int_mod: 2
  str_mod: 8
  wis_mod: 3
ac: 36
ac_special: null
alignment: CE
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 50 feet. After the dragon uses its Breath Weapon, a fierce blizzard
    of blinding snow surrounds its body for 1 round. Everything in the aura is diffcult
    terrain for other creatures that are on the ground, climbing, or flying. The blowing
    snow also makes all creatures in the area concealed. While the blizzard rages,
    the dragon's dragon chill extends to the full area of the blizzard.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Blizzard
  range: null
  raw_description: '**Blizzard** (__arcane__, __aura__, __cold__); 50 feet. After
    the dragon uses its Breath Weapon, a fierce blizzard of blinding snow surrounds
    its body for 1 round. Everything in the aura is diffcult terrain for other creatures
    that are on the ground, climbing, or flying. The blowing snow also makes all creatures
    in the area concealed. While the blizzard rages, the dragon''s dragon chill extends
    to the full area of the blizzard.'
  requirements: null
  success: null
  traits:
  - arcane
  - aura
  - cold
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 10 feet, 3d6 cold damage (DC 34 basic Reflex)
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Dragon Chill
  range: null
  raw_description: '**Dragon Chill** (__arcane__, __aura__, __cold__, __evocation__);
    10 feet, 3d6 cold damage (DC 34 basic Reflex)'
  requirements: null
  success: null
  traits:
  - arcane
  - aura
  - cold
  - evocation
  trigger: null
- action_cost: None
  critical_failure: The creature is frightened 4.
  critical_success: The creature is unaffected by the presence.
  description: 90 feet, DC 34
  effect: null
  effects: null
  failure: The creature is frightened 2.
  frequency: null
  full_description: null
  generic_description: A creature that first enters the area must attempt a Will save.
    Regardless of the result of the saving throw, the creature is temporarily immune
    to this monster's Frightful Presence for 1 minute.
  name: Frightful Presence
  range: null
  raw_description: '**Frightful Presence** 90 feet, DC 34 A creature that first enters
    the area must attempt a Will save. Regardless of the result of the saving throw,
    the creature is temporarily immune to this monster''s Frightful Presence for 1
    minute.

    Critical Success The creature is unaffected by the presence.

    Success The creature is frightened 1.

    Failure The creature is frightened 2.

    Critical Success The creature is frightened 4.'
  requirements: null
  success: The creature is frightened 1.
  traits:
  - aura
  - emotion
  - fear
  - mental
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The dragon's blood sprays on the creature, dealing 3d6 cold damage. A creature
    that takes cold damage in this way is slowed 1 for 1 round.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Freezing Blood
  range: null
  raw_description: '**Freezing Blood** [Reaction] (__arcane__, __cold__); **Trigger**
    An adjacent creature deals piercing or slashing damage to the dragon. **Effect**
    The dragon''s blood sprays on the creature, dealing 3d6 cold damage. A creature
    that takes cold damage in this way is slowed 1 for 1 round.'
  requirements: null
  success: null
  traits:
  - arcane
  - cold
  trigger: An adjacent creature deals piercing or slashing damage to the dragon.
description: 'The most feral and least intelligent of all the chromatic dragons, white
  dragons are brutish, predatory, and chiefly motivated by self-preservation. Nearly
  all other dragons look down on white dragons as hopelessly hotheaded and dull, though
  this does not make them any less dangerous—in fact, it may mean the opposite. It''s
  nearly impossible to treat with white dragons, not because they lack the capacity
  to do so, but because they just don''t care to. They are as difficult to deal with
  as they are quick to anger, and they become incensed at almost anything. When a
  white dragon does speak, it spews threats, inane jokes, and incoherent babbling
  as a prelude to attacking—and when the attack comes, it''s bloody and relentless.




  Thankfully, white dragons prefer very cold, remote locations that are far from people.
  Dwelling on glacial mountaintops or in ice caverns beneath forbidding tundra, they
  treat the lands around them as their own personal hunting grounds. They fly out
  to feed or terrorize other creatures, especially those who trespass near the dragon''s
  territory, then bring any treasure back to be displayed in icy niches in their lairs.
  They collect all kinds of valuables, with a slight preference for items with high
  utility—such as tools, trade goods, and fine food—over coins or jewels. The ability
  to shape ice lets white dragons rearrange their lairs exactly to their specifications,
  and they take great pride in how they''ve decorated their homes over the years.




  **__Recall Knowledge - Dragon__ (__Arcana__)**: DC 36'
hp: 330
hp_misc: null
immunities:
- cold
- paralyzed
- sleep
items: null
languages:
- Common
- Draconic
- Jotun
level: 15
melee:
- action_cost: One Action
  damage:
    formula: 3d12+16
    type: piercing
  name: jaws
  plus_damage:
  - formula: 3d6
    type: cold
  to_hit: 31
  traits:
  - cold
  - magical
  - reach 15 feet
- action_cost: One Action
  damage:
    formula: 3d10+16
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 31
  traits:
  - agile
  - magical
  - reach 10 feet
- action_cost: One Action
  damage:
    formula: 2d10+14
    type: bludgeoning
  name: tail
  plus_damage: null
  to_hit: 29
  traits:
  - magical
  - reach 20 feet
name: Ancient White Dragon
perception: 30
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon breathes a cloud of frost that deals 16d6 cold damage in
    a 50-foot cone (DC 36 basic Reflex save). It can't use Breath Weapon again for
    1d4 rounds.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Breath Weapon
  range: null
  raw_description: '**Breath Weapon ** [Two Actions]  (__arcane__, __cold__, __evocation__)
    The dragon breathes a cloud of frost that deals 16d6 cold damage in a 50-foot
    cone (DC 36 basic Reflex save). It can''t use Breath Weapon again for 1d4 rounds.'
  requirements: null
  success: null
  traits:
  - arcane
  - cold
  - evocation
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon makes two claw Strikes and one tail Strike in any order
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Frenzy
  range: null
  raw_description: '**Draconic Frenzy** [Two Actions]  The dragon makes two claw Strikes
    and one tail Strike in any order'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When the dragon scores a critical hit with a Strike, it recharges its
    Breath Weapon
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Momentum
  range: null
  raw_description: '**Draconic Momentum** When the dragon scores a critical hit with
    a Strike, it recharges its Breath Weapon'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The dragon slams into the ground. It can do this if it's on the ground
    Flying within 10 feet of the ground. Each creature on the ground within 10 feet
    must succeed at a DC 37 Reflex save or fall prone and take 5d6 bludgeoning damage.
    The dragon can then Step.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Ground Slam
  range: null
  raw_description: '**Ground Slam**   The dragon slams into the ground. It can do
    this if it''s on the ground Flying within 10 feet of the ground. Each creature
    on the ground within 10 feet must succeed at a DC 37 Reflex save or fall prone
    and take 5d6 bludgeoning damage. The dragon can then Step.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A white dragon can climb on ice as though it had the listed climb Speed.
    It ignores difficult terrain and greater difficult terrain from ice and snow and
    doesn't risk falling when crossing ice.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Ice Climb
  range: null
  raw_description: '**Ice Climb** A white dragon can climb on ice as though it had
    the listed climb Speed. It ignores difficult terrain and greater difficult terrain
    from ice and snow and doesn''t risk falling when crossing ice.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon reshapes a cube of ice or snow it touches, up to 10 feet
    across. Any creature standing atop the ice must succeed at a DC 15 Reflex save
    or Acrobatic check. On a failure, the creature falls prone atop the ice; on a
    critical failure, it falls off the ice entirely and is also prone.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Shape Ice
  range: null
  raw_description: '**Shape Ice** [Two Actions]  (__arcane__, __transmutation__, __water__)
    The dragon reshapes a cube of ice or snow it touches, up to 10 feet across. Any
    creature standing atop the ice must succeed at a DC 15 Reflex save or Acrobatic
    check. On a failure, the creature falls prone atop the ice; on a critical failure,
    it falls off the ice entirely and is also prone.'
  requirements: null
  success: null
  traits:
  - arcane
  - transmutation
  - water
  trigger: null
ranged: null
rarity: Uncommon
resistances:
- amount: 15
  type: fire
ritual_lists: null
saves:
  fort: 30
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 26
  ref_misc: null
  will: 24
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: Snow doesn't impair a white dragon's vision; it ignores concealment
    from snowfall.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Snow Vision
  range: null
  raw_description: '**Snow Vision** Snow doesn''t impair a white dragon''s vision;
    it ignores concealment from snowfall.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +30
- darkvision
- scent (imprecise) 60 feet
- snow vision
size: Huge
skills:
- bonus: 24
  misc: null
  name: 'Acrobatics '
- bonus: 23
  misc: null
  name: 'Arcana '
- bonus: 31
  misc: null
  name: 'Athletics '
- bonus: 28
  misc: null
  name: 'Intimidation '
- bonus: 26
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary
  page_start: 115
  page_stop: null
speed:
- amount: 40
  type: Land
- amount: 160
  type: fly
- amount: 40
  type: ice climb
spell_lists:
- dc: 36
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: null
    level: 6
    spells:
    - frequency: at will
      name: wall of ice
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: at will
      name: obscuring mist
      requirement: null
  - heightened_level: null
    level: 1
    spells:
    - frequency: at will
      name: gust of wind
      requirement: null
  to_hit: null
traits:
- Uncommon
- CE
- Huge
- Cold
- Dragon
type: Creature
weaknesses: null
